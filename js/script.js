new Promise(function(resolve){
	if (document.readyState === 'complete') {
		resolve();
	} else {
		window.onload = resolve;
	}
}).then(function(){
	return new Promise(function(resolve,reject){
		VK.init({
			apiId: 5375547
		});

		VK.Auth.login(function(response){
			if (response.session) {
				resolve(response);
			} else {
				reject(new Error('Не удалось авторизоваться'));
			}
		});
	});
}).then(function(){
	return new Promise(function(resolve,reject){
		VK.api('friends.get',{'fields':"photo_50"},function(response){
			if (response.error) {
				reject(new Error(response.error.error_msg));
			} else {
				var source = document.getElementById('content-list').innerHTML,
						friendsServer = document.getElementById('friends-server'),
						friendsLocal = document.getElementById('friends-local'),
						searchServer = document.getElementById('search-server'),
						searchLocal = document.getElementById('search-local'),
						templateFn = Handlebars.compile(source),
						dragItems,
						serverAnswer = response.response;
						localAnswer = [],
						buttonSave = document.getElementById('save-local-friends');
							
						// Вывод списка друзей по шаблону
						function renderList(data,list,actionList) {
							template = templateFn({list: data, action: actionList});
							list.innerHTML = template;
						}

						// Обработка события поиска по списка
						function eventSearch(inputSearch,arrSearch,listOutput,action) {
							var searchQuery = inputSearch.value.trim().toLowerCase();
							var searchAsnwer = arrSearch.filter(function(item){
								var fullName = item.first_name + " "+item.last_name;
								fullName = fullName.trim().toLowerCase();
								return fullName.indexOf(searchQuery) >= 0;
							});

							renderList(searchAsnwer,listOutput,action);
						}

						// Обработка добавления/удаления друзей
						function addRemoveAction(arrServer,arrLocal,event,userId) {

							// Если нет userId, обрабатываем событие клика
							// В противном случае это перетаскивание

							if (userId == undefined) {
								var target = event.target;
								if (!target.classList.contains('content__item-action')) return;

								var parent = target.closest('.content__item');
								var userId = parent.getAttribute('data-id');	
							}

							var friendObj,delFrom,addTo,delPosition;

							// Проходимся по каждому из объектов
							// Ищем в каком из массив есть друг
							// Записываем его данные в новый
							// Независимый объект
							for(var i = 0; i < arrServer.length; i++) {
								if(arrServer[i].uid == userId) {
									friendObj = arrServer[i];
									delFrom = "server";
									addTo = "local";
									delPosition = i;
									break;
								}
							}

							for(var i = 0; i < arrLocal.length; i++) {
								if(arrLocal[i].uid == userId) {
									friendObj = arrLocal[i];
									delFrom = "local";
									addTo = "server";
									delPosition = i;
									break;
								}
							}

							//Подготавливаем параметры для добавления нового
							//друга

							var newFriend = {
								'first_name': friendObj.first_name,
								'last_name': friendObj.last_name,
								'photo_50': friendObj.photo_50,
								'uid': userId,
							};

							//Удаляем и добавляем объект в зависимости
							//от собранных параметров
							if (delFrom == "server" && addTo == "local") {
								arrServer.splice(delPosition,1);
								arrLocal.splice(0,0,newFriend);
							} else if (delFrom == "local" && addTo == "server") {
								arrLocal.splice(delPosition,1);
								arrServer.splice(0,0,newFriend);
							}

							// Пересобираем списки друзей

							renderList(arrLocal,friendsLocal,'del');
							renderList(arrServer,friendsServer,'add');

							// Устанавливаем события drag'n'drop
							setDragDrop();
						}	

						// Обработка получения локальных друзей
						function setLocalFriends() {
							for (var prop in localStorage) {
								var friendObj,
										delPosition;

								// Находим данные по другу в ответе с сервера

								userId = localStorage[prop];

								for(var i = 0; i < serverAnswer.length; i++) {
									if (serverAnswer[i].uid == userId) {
										friendObj = {
											'first_name': serverAnswer[i].first_name,
											'last_name': serverAnswer[i].last_name,
											'photo_50': serverAnswer[i].photo_50,
											'uid': serverAnswer[i].uid,
										}

										delPosition = i;
									}
								}

								if(!friendObj) {
									// Ситуация, когда мы удалили друга,
									// а он может оставаться в локальном списке
									return;
								}

								// Удаляем друга из серверного списка
								serverAnswer.splice(delPosition,1);

								//Добавляем в локальный список друзей
								localAnswer.splice(0,0,friendObj);
							}
						}

						// Обработка событий перетаскивания
						function setDragDrop() {

							// Получаем список элементов, которые можно перетаскивать
							dragItems = document.querySelectorAll('#friends-server .content__item');
							dropBlock = document.querySelector('#friends-local .content__list');

							// Обработка события перетаскивания
							[].forEach.call(dragItems, function(item) {
								// drag start
								item.addEventListener('dragstart', function(e){

	  							e.dataTransfer.effectAllowed = 'move';
	  							e.dataTransfer.setData('text/plain', this.getAttribute('data-id'));

								}, false);
							});

							// Обработка списка-приемника перетаскивания
							dropBlock.addEventListener('dragover', function(e){
								e.preventDefault();
							}, false);

							dropBlock.addEventListener('drop', function(e){
								if (e.stopPropagation) {
    							e.stopPropagation();	    							
  							}

  							userId = e.dataTransfer.getData('text/plain');

  							addRemoveAction(serverAnswer,localAnswer,"",userId);
  							
  							return false;
								}, false);
						}

						// Поиск по серверному списку друзей
						searchServer.addEventListener('keyup',function(){
							eventSearch(searchServer,serverAnswer,friendsServer,"add");
						});

						// Поиск по локальному списку друзей
						searchLocal.addEventListener('keyup',function(){
							eventSearch(searchLocal,localAnswer,friendsLocal,"del");
						});
						
						// Обработка клика по кнопке добавить в локальные друзья
						friendsServer.addEventListener('click',function(){
							addRemoveAction(serverAnswer,localAnswer,event);
						});

						// Обработка клика по кнопке удалить из локальных друзей
						friendsLocal.addEventListener('click',function(){
							addRemoveAction(serverAnswer,localAnswer,event);
						});

						// Записываем добавленных друзей в localStorage
						buttonSave.addEventListener('click',function(){
							// Очищаем текущий локальный список
							localStorage.clear();

							// Проходимся по списку локальных друзей 
							// и добавляем их в localStorage
							localAnswer.forEach(function(item,i){
								localStorage.setItem('localFriend'+i,item.uid);								
							});

							alert("Сохранено");
						});

						// Получаем список локальных друзей
						setLocalFriends();

						// Отрисовка списков друзей
						renderList(serverAnswer,friendsServer,'add');
						renderList(localAnswer,friendsLocal,'del');

						// Инициализация событий drag'n'drop
						setDragDrop();

						resolve();
			}
		});
	});
})
.catch(function(e){
	alert("Ошибка: "+e.message);
});